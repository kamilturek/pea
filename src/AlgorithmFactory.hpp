#pragma once
#include "BranchAndBound.hpp"
#include "BruteForce.hpp"
#include "DynamicProgramming.hpp"
#include "GeneticAlgorithm.hpp"
#include "IAlgorithm.hpp"
#include "MultithreadedTabuSearch.hpp"
#include "TabuSearch.hpp"
#include <map>
#include <memory>

class AlgorithmFactory
{
public:
    enum class Algorithm {
        BruteForce,
        BranchAndBound,
        DynamicProgramming,
        TabuSearch,
        MultithreadedTabuSearch,
        GeneticAlgorithm
    };

    std::unique_ptr<IAlgorithm> createAlgorithm(Algorithm algorithm, const Matrix<int>& adjacencyMatrix);

private:
    std::map<Algorithm, std::function<std::unique_ptr<IAlgorithm>(const Matrix<int>&)>> _algorithms = {
        { Algorithm::BruteForce,
          [](const Matrix<int>& adjacencyMatrix) { return std::make_unique<BruteForce>(adjacencyMatrix); } },
        { Algorithm::BranchAndBound,
          [](const Matrix<int>& adjacencyMatrix) { return std::make_unique<BranchAndBound>(adjacencyMatrix); } },
        { Algorithm::DynamicProgramming,
          [](const Matrix<int>& adjacencyMatrix) { return std::make_unique<DynamicProgramming>(adjacencyMatrix); } },
        { Algorithm::TabuSearch,
          [](const Matrix<int>& adjacencyMatrix) { return std::make_unique<TabuSearch>(adjacencyMatrix); } },
        { Algorithm::MultithreadedTabuSearch,
          [](const Matrix<int>& adjacencyMatrix) { return std::make_unique<MultithreadedTabuSearch>(adjacencyMatrix); } },
        { Algorithm::GeneticAlgorithm,
          [](const Matrix<int>& adjacencyMatrix) { return std::make_unique<GeneticAlgorithm>(adjacencyMatrix); } },
    };
};