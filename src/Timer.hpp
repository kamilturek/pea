#pragma once
#include <chrono>

class Timer
{
public:
    void start();
    std::chrono::microseconds stop();
    bool elapsed(std::chrono::seconds seconds) const;
    bool elapsed(std::chrono::milliseconds miliseconds) const;

private:
    std::chrono::time_point<std::chrono::steady_clock> _start;
};