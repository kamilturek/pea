#pragma once
#include "Matrix.hpp"
#include "IAlgorithm.hpp"
#include <random>
#include <memory>
#include <optional>

class MatrixGenerator
{
public:
    MatrixGenerator();
    Matrix<int> generateMatrix(const int size);

private:
    std::random_device _rd;
    std::optional<std::mt19937> _gen;
    Matrix<int> _randomMatrix;

    int getRandomNumber(const int min, const int max);
    void fillMatrix();
};