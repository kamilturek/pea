#pragma once
#include <vector>
#include <iostream>

template <typename T>
using Matrix = std::vector<std::vector<T>>;

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& matrix)
{
    for (const auto& row : matrix)
    {
        for (const auto& elem : row)
            os << elem << '\t';
        os << '\n';
    }

    return os;   
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    for (const auto& elem : vec)
        std::cout << elem << ' ';

    return os;
}