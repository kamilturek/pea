#pragma once
#include <algorithm>
#include <fstream>
#include <string>
#include <vector>
#include "Matrix.hpp"

class MatrixFileInput
{
public:
    explicit MatrixFileInput(const std::string& filepath, int metadataLinesCount, int size);

    Matrix<int> readMatrix();

private:
    unsigned int _metadataLinesCount;
    unsigned int _size;
    std::ifstream _inputStream;

    void skipLines(const unsigned int linesCount);
    void setToBeginning();
};