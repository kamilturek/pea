#include "Timer.hpp"

void Timer::start()
{
    _start = std::chrono::steady_clock::now();
}

std::chrono::microseconds Timer::stop()
{
    return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - _start);
}

bool Timer::elapsed(std::chrono::seconds seconds) const
{
    return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - _start) > seconds;
}

bool Timer::elapsed(std::chrono::milliseconds miliseconds) const
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _start) > miliseconds;
}