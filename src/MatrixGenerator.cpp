#include "MatrixGenerator.hpp"

MatrixGenerator::MatrixGenerator() : _gen(_rd())
{
}

Matrix<int> MatrixGenerator::generateMatrix(const int size)
{
    _randomMatrix = Matrix<int>(size, std::vector<int>(size));
    fillMatrix();

    return _randomMatrix;
}

int MatrixGenerator::getRandomNumber(const int min, const int max)
{
    std::uniform_int_distribution dis(min, max);
    return dis(_gen.value());
}

void MatrixGenerator::fillMatrix()
{
    const int min = 0;
    const int max = 20;

    for (size_t i = 0; i < _randomMatrix.size(); i++)
        for (size_t j = 0; j < _randomMatrix[i].size(); j++)
        {
            if (i == j)
                _randomMatrix[i][j] = IAlgorithm::INF;
            else
                _randomMatrix[i][j] = getRandomNumber(min, max);
        }
}