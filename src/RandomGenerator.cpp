#include "RandomGenerator.hpp"

RandomGenerator::RandomGenerator() : _gen(_rd())
{
}

float RandomGenerator::randomFloat(float min, float max)
{
    std::uniform_real_distribution<> dis(min, max);
    return dis(_gen);
}

int RandomGenerator::randomInt(int min, int max)
{
    std::uniform_int_distribution<> dis(min, max);
    return dis(_gen);
}

int RandomGenerator::randomPop(std::vector<int>& choices)
{
    std::uniform_int_distribution<> dis(0, choices.size() - 1);

    int chosenIndex = dis(_gen);
    int choice = choices[chosenIndex];
    choices.erase(choices.begin() + chosenIndex);
    
    return choice;
}