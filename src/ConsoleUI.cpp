#include "ConsoleUI.hpp"

void ConsoleUI::run()
{
    runMainMenu();
}

void ConsoleUI::runMainMenu()
{
    displayHeader();

    while (true)
    {
        std::cout << "1) Wczytaj dane testowe z pliku\n"
                  << "2) Wygeneruj dane testowe\n"
                  << "3) Wyświetl wczytane dane\n"
                  << "4) Przegląd zupełny\n"
                  << "5) Algorytm podziału i ograniczeń\n"
                  << "6) Programowanie dynamiczne\n"
                  << "7) Przeszukiwanie Tabu\n"
                  << "8) Wielowątkowe przeszukiwanie Tabu\n"
                  << "9) Algorytm genetyczny\n"
                  << "10) Wyjdź z programu\n";

        switch (getChoice(1, 11))
        {
        case 1:
            readFile();
            break;
        case 2:
            generateData();
            break;
        case 3:
            displayLoadedMatrix();
            break;
        case 4:
            solve(AlgorithmFactory::Algorithm::BruteForce);
            break;
        case 5:
            solve(AlgorithmFactory::Algorithm::BranchAndBound);
            break;
        case 6:
            solve(AlgorithmFactory::Algorithm::DynamicProgramming);
            break;
        case 7:
            solve(AlgorithmFactory::Algorithm::TabuSearch);
            break;
        case 8:
            solve(AlgorithmFactory::Algorithm::MultithreadedTabuSearch);
            break;
        case 9:
            solve(AlgorithmFactory::Algorithm::GeneticAlgorithm);
            break;
        case 10:
            exit(0);
        }
    }
}

void ConsoleUI::readFile()
{
    unsigned int lineSkipCount =
        getFromUser<int>("Liczba linii do pominięcia:");
    unsigned int matrixDimension = getFromUser<int>("Wprowadź liczbę miast:");
    std::string filepath = getFromUser<std::string>("Ścieżka do pliku:");

    try
    {
        MatrixFileInput input(filepath, lineSkipCount, matrixDimension);
        _adjacencyMatrix = input.readMatrix();
        std::cout << "Wczytano plik.\n";
    }
    catch (const std::invalid_argument& ex)
    {
        std::cout << ex.what() << '\n';
    }
}

void ConsoleUI::generateData()
{
    int matrixSize = getFromUser<int>("Liczba miast:");

    if (matrixSize < 0)
    {
        std::cout << "BŁĄD: Liczba miast nie może być ujemna.\n";
        return;
    }

    _adjacencyMatrix = MatrixGenerator().generateMatrix(matrixSize);
    std::cout << "Wygenerowano dane.\n";
}

void ConsoleUI::solve(AlgorithmFactory::Algorithm choosenAlgorithm)
{
    if (!_adjacencyMatrix.has_value())
    {
        std::cout << "BŁĄD: Nie wczytano żadnego pliku.\n";
        return;
    }

    auto algorithm = AlgorithmFactory().createAlgorithm(choosenAlgorithm, _adjacencyMatrix.value());

    try
    {
        Timer timer;
        timer.start();
        std::pair<int, std::vector<int>> result = algorithm->solve();
        auto elapsedTime = timer.stop();
        std::cout << "Wynik: " << result.first << '\n'
                  << "Ścieżka: " << result.second << '\n'
                  << "Czas: " << elapsedTime.count() << " [µs]\n";
    }
    catch (const std::invalid_argument& ex)
    {
        std::cout << ex.what() << '\n';
    }
}

int ConsoleUI::getChoice(int lowerBound, int upperBound) const
{
    int choice;
    do
    {
        std::cout << ">> ";
        std::cin >> choice;

        if (choice < lowerBound || choice >= upperBound)
            std::cout
                << "BŁĄD: Wybrano niedostępną operację. Spróbuj ponownie!\n";
    } while (choice < lowerBound || choice >= upperBound);

    std::cout << '\n';

    return choice;
}

void ConsoleUI::displayHeader() const
{
    std::cout << "PROJEKTOWANIE EFEKTYWNYCH ALGORYTMÓW\n"
              << "KAMIL TUREK 2019\n\n";
}

void ConsoleUI::displayLoadedMatrix() const
{
    if (!_adjacencyMatrix.has_value())
    {
        std::cout << "BŁĄD: Nie wczytano żadnego pliku.\n";
        return;
    }

    std::cout << (_adjacencyMatrix.value());
}