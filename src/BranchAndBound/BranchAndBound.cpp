#include "BranchAndBound.hpp"

BranchAndBound::BranchAndBound(const Matrix<int>& adjacencyMatrix) : _adjacencyMatrix(adjacencyMatrix)
{
    if (adjacencyMatrix.size() == 0 || std::any_of(adjacencyMatrix.begin(), adjacencyMatrix.end(),
                                                   [&](const auto& row) { return row.size() != adjacencyMatrix.size(); }))
        throw std::invalid_argument("BŁĄD: Niepoprawna macierz.");
}

std::pair<int, std::vector<int>> BranchAndBound::solve()
{
    const int level = 0;
    const int sourceVertex = -1;
    const int targetVertex = 0;
    const int initCost = 0;
    const std::vector<std::pair<int, int>> path;


    int bestBound = IAlgorithm::INF;
    std::vector<int> bestPath;

    Node root(_adjacencyMatrix, path, initCost, level, sourceVertex, targetVertex);
    _liveNodes.push(root);

    while (!_liveNodes.empty())
    {
        auto lowestCostNode = popLowestCostNode();

        if (lowestCostNode.getLevel() == _adjacencyMatrix.size() - 1)
        {
            lowestCostNode.insertToPath(std::make_pair(lowestCostNode.getVertexID(), 0));
            const int cost = lowestCostNode.getCost();
            if (cost < bestBound)
            {
                bestBound = cost;
                bestPath = lowestCostNode.getReducedPath();
            }
        }
        else if (lowestCostNode.getCost() < bestBound)
            for (size_t targetVertexID = 0; targetVertexID < _adjacencyMatrix.size(); targetVertexID++)
            {
                if (lowestCostNode.getReducedMatrix()[lowestCostNode.getVertexID()][targetVertexID] != IAlgorithm::INF)
                {
                    Node child = lowestCostNode.makeChild(targetVertexID);
                    _liveNodes.push(child);
                }
            }
    }

    return std::make_pair(bestBound, bestPath);
}

Node BranchAndBound::popLowestCostNode()
{
    auto lowestCostNode = _liveNodes.top();
    _liveNodes.pop();
    return lowestCostNode;
}