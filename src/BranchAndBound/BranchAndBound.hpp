#pragma once
#include "IAlgorithm.hpp"
#include "Matrix.hpp"
#include "Node.hpp"
#include <functional>
#include <queue>

class BranchAndBound : public IAlgorithm
{
public:
    explicit BranchAndBound(const Matrix<int>& adjacencyMatrix);
    std::pair<int, std::vector<int>> solve() override;

private:
    Matrix<int> _adjacencyMatrix;
    std::priority_queue<Node, std::vector<Node>, std::greater<Node>> _liveNodes;

    Node popLowestCostNode();
};