#pragma once
#include <algorithm>
#include <functional>
#include <list>
#include <numeric>
#include <vector>
#include "IAlgorithm.hpp"
#include "Matrix.hpp"

class Node
{
public:
    Node(const Matrix<int>& adjacencyMatrix, const std::vector<std::pair<int, int>>& path, const int accessCost,
         const unsigned int level, int sourceVertex, int targetVertex);

    void insertToPath(const std::pair<int, int> &elem);
    Node makeChild(const int targetVertex) const;

    int getCost() const;
    int getVertexID() const;
    unsigned int getLevel() const;
    Matrix<int> getReducedMatrix() const;
    std::vector<int> getReducedPath() const;
    std::vector<std::pair<int, int>> getPath() const;

    bool operator>(const Node &other) const;

private:
    int _cost;
    int _accessCost;
    int _vertexID;
    unsigned int _level;

    Matrix<int> _reducedMatrix;
    std::vector<std::pair<int, int>> _path;
    std::function<int(int, int)> _accumulateOperator = [](int sum, int value) { return (value != IAlgorithm::INF) ? sum + value : sum; };

    int reduceRows();
    int reduceColumns();

    void computeCost();
    void removeEdges(const int sourceVertex, const int targetVertex);
};