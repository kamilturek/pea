#include "Node.hpp"

Node::Node(const Matrix<int>& adjacencyMatrix, const std::vector<std::pair<int, int>>& path,
           const int accessCost, const unsigned int level, const int sourceVertex,
           const int targetVertex)
    : _cost(0), _accessCost(accessCost), _vertexID(targetVertex), _level(level),
      _reducedMatrix(adjacencyMatrix), _path(path)
{
    if (level != 0)
    {
        _path.push_back(std::make_pair(sourceVertex, targetVertex));
        removeEdges(sourceVertex, targetVertex);
    }

    computeCost();
}

void Node::computeCost()
{
    _cost = _accessCost + (reduceRows() + reduceColumns());
}

void Node::removeEdges(const int sourceVertex, const int targetVertex)
{
    _reducedMatrix[targetVertex][0] = IAlgorithm::INF;

    for (size_t i = 0; i < _reducedMatrix.size(); i++)
    {
        _reducedMatrix[sourceVertex][i] = IAlgorithm::INF;
        _reducedMatrix[i][targetVertex] = IAlgorithm::INF;
    }
}

int Node::reduceRows()
{
    std::vector<int> rowMinimums(_reducedMatrix.size(), IAlgorithm::INF);

    for (size_t i = 0; i < _reducedMatrix.size(); i++)
        for (size_t j = 0; j < _reducedMatrix[i].size(); j++)
            if (_reducedMatrix[i][j] < rowMinimums[i])
                rowMinimums[i] = _reducedMatrix[i][j];

    for (size_t i = 0; i < _reducedMatrix.size(); i++)
        for (size_t j = 0; j < _reducedMatrix[i].size(); j++)
            if (_reducedMatrix[i][j] != IAlgorithm::INF && rowMinimums[i] != IAlgorithm::INF)
                _reducedMatrix[i][j] -= rowMinimums[i];

    return std::accumulate(rowMinimums.begin(), rowMinimums.end(), 0, _accumulateOperator);
}

int Node::reduceColumns()
{
    std::vector<int> columnMinimums(_reducedMatrix.size(), IAlgorithm::INF);

    for (size_t i = 0; i < _reducedMatrix.size(); i++)
        for (size_t j = 0; j < _reducedMatrix[i].size(); j++)
            if (_reducedMatrix[i][j] < columnMinimums[j])
                columnMinimums[j] = _reducedMatrix[i][j];

    for (size_t i = 0; i < _reducedMatrix.size(); i++)
        for (size_t j = 0; j < _reducedMatrix[i].size(); j++)
            if (_reducedMatrix[i][j] != IAlgorithm::INF && columnMinimums[j] != IAlgorithm::INF)
                _reducedMatrix[i][j] -= columnMinimums[j];

    return std::accumulate(columnMinimums.begin(), columnMinimums.end(), 0, _accumulateOperator);
}

Node Node::makeChild(const int targetVertex) const
{
    int accessCost = getCost() + getReducedMatrix()[getVertexID()][targetVertex];

    return Node(getReducedMatrix(), getPath(), accessCost, getLevel() + 1, getVertexID(),
                targetVertex);
}

void Node::insertToPath(const std::pair<int, int>& elem)
{
    _path.push_back(elem);
}

int Node::getCost() const
{
    return _cost;
}

unsigned int Node::getLevel() const
{
    return _level;
}

int Node::getVertexID() const
{
    return _vertexID;
}

std::vector<int> Node::getReducedPath() const
{
    std::vector<int> reducedPath;
    std::transform(_path.begin(), _path.end(), std::inserter(reducedPath, reducedPath.begin()),
                   [](const std::pair<int, int>& edge) { return edge.first; });

    reducedPath.push_back(_path.back().second);

    return reducedPath;
}

std::vector<std::pair<int, int>> Node::getPath() const
{
    return _path;
}

Matrix<int> Node::getReducedMatrix() const
{
    return _reducedMatrix;
}

bool Node::operator>(const Node& rhs) const
{
    return _cost > rhs._cost;
}