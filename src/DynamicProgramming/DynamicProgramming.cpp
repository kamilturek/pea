#include "DynamicProgramming.hpp"

DynamicProgramming::DynamicProgramming(const Matrix<int>& adjacencyMatrix)
    : _fullMask((1 << _adjacencyMatrix.size()) - 1), _adjacencyMatrix(adjacencyMatrix)
{
}

std::pair<int, std::vector<int>> DynamicProgramming::solve()
{
    _stateMatrix.clear();
    _stateMatrix.resize(_adjacencyMatrix.size(), std::vector<int>((1 << _adjacencyMatrix.size()) - 1, IAlgorithm::INF));
    _travelMatrix.clear();
    _travelMatrix.resize(_adjacencyMatrix.size(), std::vector<int>((1 << _adjacencyMatrix.size()) - 1, IAlgorithm::INF));

    return std::make_pair(getCost(0, 1), std::vector<int>());
}

int DynamicProgramming::getCost(const int sourceVertex, const int visitedMask)
{
    if (visitedMask == _fullMask)
        return _adjacencyMatrix[sourceVertex][0];

    if (_stateMatrix[sourceVertex][visitedMask] != IAlgorithm::INF)
        return _stateMatrix[sourceVertex][visitedMask];

    for (size_t i = 0; i < _adjacencyMatrix.size(); i++)
    {
        if (static_cast<int>(i) == sourceVertex || (visitedMask & (1 << i)))
            continue;

        int cost = _adjacencyMatrix[sourceVertex][i] + getCost(i, visitedMask | (1 << i));
        if (cost < _stateMatrix[sourceVertex][visitedMask])
            _stateMatrix[sourceVertex][visitedMask] = cost;
    }

    return _stateMatrix[sourceVertex][visitedMask];
}