#pragma once
#include "IAlgorithm.hpp"
#include <algorithm>

class DynamicProgramming : public IAlgorithm
{
public:
    explicit DynamicProgramming(const Matrix<int>& adjacencyMatrix);

    std::pair<int, std::vector<int>> solve() override;

private:
    int _fullMask;

    Matrix<int> _stateMatrix;
    Matrix<int> _adjacencyMatrix;
    Matrix<int> _travelMatrix;

    int getCost(const int sourceVertex, const int visitedMask);
    std::vector<int> getPath() const;
};