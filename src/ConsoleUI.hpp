#pragma once
#include "AlgorithmFactory.hpp"
#include "Matrix.hpp"
#include "MatrixFileInput.hpp"
#include "MatrixGenerator.hpp"
#include "Timer.hpp"
#include <iostream>
#include <memory>
#include <optional>

class ConsoleUI
{
public:
    void run();

private:
    std::optional<Matrix<int>> _adjacencyMatrix;

    void runMainMenu();

    void displayHeader() const;
    void displayLoadedMatrix() const;
    void displayPath(const std::vector<int>& path) const;

    void solve(AlgorithmFactory::Algorithm algorithm);

    void readFile();
    void generateData();

    int getChoice(int lowerBound, int upperBound) const;

    template <typename T> T getFromUser(std::string_view prompt) const
    {
        T input;
        std::cout << prompt << '\n' << ">> ";
        std::cin >> input;
        std::cout << '\n';

        return input;
    }
};