#pragma once
#include <random>
#include <algorithm>

class RandomGenerator
{
public:
    RandomGenerator();
    float randomFloat(float min, float max);
    int randomInt(int min, int max);
    int randomPop(std::vector<int>& choices);

private:
    std::random_device _rd;
    std::mt19937 _gen;
};