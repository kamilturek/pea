#include "AlgorithmFactory.hpp"

std::unique_ptr<IAlgorithm> AlgorithmFactory::createAlgorithm(AlgorithmFactory::Algorithm algorithm,
                                                              const Matrix<int>& adjacencyMatrix)
{
    return _algorithms.at(algorithm)(adjacencyMatrix);
}