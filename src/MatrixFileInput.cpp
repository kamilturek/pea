#include "MatrixFileInput.hpp"

MatrixFileInput::MatrixFileInput(const std::string &filepath, int metadataLinesCount, int size)
    : _metadataLinesCount(metadataLinesCount), _size(size)
{
    if (metadataLinesCount < 0)
        throw std::invalid_argument("BŁĄD: Liczba linii nie może być ujemna.");
    if (size < 0)
        throw std::invalid_argument("BŁĄD: Rozmiar macierzy nie może być liczbą ujemną.");

    _inputStream.open(filepath);

    if (!_inputStream.is_open())
        throw std::invalid_argument("BŁĄD: Nie znaleziono pliku: " + filepath);
}

Matrix<int> MatrixFileInput::readMatrix()
{
    skipLines(_metadataLinesCount);
    Matrix<int> matrix(_size);

    for (unsigned int i = 0; i < _size; i++)
    {
        matrix[i].reserve(_size);
        for (unsigned int j = 0; j < _size; j++)
        {
            if (!_inputStream)
                throw std::invalid_argument("BŁĄD: Plik zawiera niewystarczającą liczbę elementów.");

            int matrixElement;
            _inputStream >> matrixElement;
            matrix[i].push_back(matrixElement);
        }
    }

    setToBeginning();

    return matrix;
}

void MatrixFileInput::skipLines(const unsigned int linesCount)
{
    std::string trash;
    for (unsigned int i = 0; i < linesCount; i++)
        std::getline(_inputStream, trash);
}

void MatrixFileInput::setToBeginning()
{
    _inputStream.seekg(_inputStream.beg);
}