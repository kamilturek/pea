#pragma once
#include "IAlgorithm.hpp"
#include "TabuSearch.hpp"
#include <future>
#include <algorithm>

class MultithreadedTabuSearch : public IAlgorithm
{
public:
    explicit MultithreadedTabuSearch(const Matrix<int>& adjacencyMatrix);

    std::pair<int, std::vector<int>> solve() override;
    void setTimeLimit(std::chrono::seconds seconds);

private:
    std::chrono::seconds _timeLimit = std::chrono::seconds(0);

    int _threadsNumber = 4;
    Matrix<int> _adjacencyMatrix;

    std::pair<int, std::vector<int>> runTabuSearch();
};