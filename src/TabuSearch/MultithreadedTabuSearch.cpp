#include "MultithreadedTabuSearch.hpp"
#include <iostream>

MultithreadedTabuSearch::MultithreadedTabuSearch(const Matrix<int>& adjacencyMatrix) : _adjacencyMatrix(adjacencyMatrix)
{
}

std::pair<int, std::vector<int>> MultithreadedTabuSearch::solve()
{
    std::vector<std::pair<int, std::vector<int>>> results;
    std::vector<std::future<std::pair<int, std::vector<int>>>> threads;

    for (int i = 0; i < _threadsNumber; i++)
        threads.push_back(std::async(std::launch::async, &MultithreadedTabuSearch::runTabuSearch, this));

    for (int i = 0; i < _threadsNumber; i++)
        results.push_back(threads[i].get());

    return *std::min_element(results.begin(), results.end(),
                             [](const auto& result1, const auto& result2) { return result1.first < result2.first; });
}

std::pair<int, std::vector<int>> MultithreadedTabuSearch::runTabuSearch()
{
    TabuSearch tabuSearch(_adjacencyMatrix);
    if (_timeLimit != std::chrono::seconds(0))
        tabuSearch.setTimeLimit(_timeLimit);

    return tabuSearch.solve();
}

void MultithreadedTabuSearch::setTimeLimit(std::chrono::seconds seconds)
{
    _timeLimit = seconds;
}