#pragma once
#include <optional>
#include <vector>

class Solution
{
public:
    int _cost;
    std::vector<int> _path;
    std::optional<std::pair<int, int>> _move;

    Solution();
    explicit Solution(const std::vector<int>& path);
    Solution(const std::vector<int>& path, const std::pair<int, int>& move);

    bool operator<(const Solution& other) const;
};