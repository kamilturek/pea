#pragma once
#include <algorithm>
#include "Matrix.hpp"

class TabuList
{
public:
    TabuList(int size, int tenure);

    void update();
    void addTabu(const std::pair<int, int>& move);
    void divideBy(int divisor);
    bool isTabu(const std::pair<int, int>& move) const;

private:
    int _tenure;
    Matrix<int> _tabuList;
};