#include "TabuSearch.hpp"

TabuSearch::TabuSearch(const Matrix<int>& adjacencyMatrix)
    : _adjacencyMatrix(adjacencyMatrix),
      _tabuList(adjacencyMatrix.size(), adjacencyMatrix.size() * 2)
{
}

std::pair<int, std::vector<int>> TabuSearch::solve()
{
    Timer _timer;
    _timer.start();

    Solution currentSolution;
    currentSolution._path = generateGreedyPath();
    currentSolution._cost = calculateCost(currentSolution._path);
    _bestSolution = currentSolution;

    while (!_timer.elapsed(_timeLimit))
    {
        currentSolution = getBestNeighbourSolution(currentSolution._path);
        if (currentSolution._cost < _bestSolution._cost)
        {
            _bestSolution = currentSolution;
            _iterationsSinceImprovement = 0;
            _tabuList.divideBy(4);
        }
        else
            _iterationsSinceImprovement++;

        _tabuList.update();
        _tabuList.addTabu(currentSolution._move.value());

        if (_iterationsSinceImprovement > _diversificationLimit)
        {
            _iterationsSinceImprovement = 0;
            currentSolution._move.reset();
            currentSolution._path = generateGreedyRandomPath();
            currentSolution._cost = calculateCost(currentSolution._path);

            if (currentSolution._cost < _bestSolution._cost)
                _bestSolution = currentSolution;
        }
    }

    return std::make_pair(_bestSolution._cost, _bestSolution._path);
}

std::vector<int> TabuSearch::generateGreedyPath() const
{
    const int startVertex = RandomGenerator().randomInt(0, _adjacencyMatrix.size() - 1);

    std::vector<int> path = { startVertex };

    while (path.size() < _adjacencyMatrix.size())
    {
        const std::vector<int>& currentVertexNeighbours = _adjacencyMatrix[path.back()];

        int nearestNeighbour = -1;
        int nearestNeighbourDistance = std::numeric_limits<int>::max();

        for (size_t neighbour = 0; neighbour < currentVertexNeighbours.size(); neighbour++)
        {
            if (!isInPath(neighbour, path))
            {
                const int neighbourDistance = currentVertexNeighbours[neighbour];
                if (neighbourDistance < nearestNeighbourDistance)
                {
                    nearestNeighbour = neighbour;
                    nearestNeighbourDistance = neighbourDistance;
                }
            }
        }

        path.push_back(nearestNeighbour);
    }

    path.push_back(startVertex);
    return path;
}

std::vector<int> TabuSearch::generateGreedyRandomPath() const
{
    std::vector<int> vertices(_adjacencyMatrix.size());
    std::iota(vertices.begin(), vertices.end(), 0);

    RandomGenerator randomGenerator;
    int randomVerticesCount = randomGenerator.randomInt(1, _adjacencyMatrix.size());
    
    std::vector<int> path;

    for (int i = 0; i < randomVerticesCount; i++)
        path.push_back(randomGenerator.randomPop(vertices));
    
    while (path.size() < _adjacencyMatrix.size())
    {
        const std::vector<int>& currentVertexNeighbours = _adjacencyMatrix[path.back()];

        int nearestNeighbour = -1;
        int nearestNeighbourDistance = std::numeric_limits<int>::max();

        for (size_t neighbour = 0; neighbour < currentVertexNeighbours.size(); neighbour++)
        {
            if (!isInPath(neighbour, path))
            {
                const int neighbourDistance = currentVertexNeighbours[neighbour];
                if (neighbourDistance < nearestNeighbourDistance)
                {
                    nearestNeighbour = neighbour;
                    nearestNeighbourDistance = neighbourDistance;
                }
            }
        }

        path.push_back(nearestNeighbour);
    }

    path.push_back(*path.begin());
    return path;
}

Solution TabuSearch::getBestNeighbourSolution(const std::vector<int>& path)
{
    auto candidates = getCandidates(path);
    for (auto& candidate : candidates)
        candidate._cost = calculateCost(candidate._path);
    std::sort(candidates.begin(), candidates.end());

    while (!candidates.empty())
    {
        Solution bestCandidate = *candidates.begin();

        if (_tabuList.isTabu(bestCandidate._move.value()))
        {
            if (isInAspirationCriteria(bestCandidate._cost))
                return bestCandidate;

            candidates.erase(candidates.begin());
            continue;
        }

        return bestCandidate;
    }

    throw std::logic_error("No candidate found.");
}

std::vector<Solution> TabuSearch::getCandidates(const std::vector<int>& path)
{
    std::vector<Solution> candidates;

    for (size_t i = 1; i < path.size() - 1; i++)
        for (size_t j = 1; j < path.size() - 1; j++)
            if (i < j)
            {
                std::vector<int> candidatePath(path);
                std::swap(candidatePath[i], candidatePath[j]);

                auto move = std::make_pair(i, j);
                candidates.push_back(Solution(candidatePath, move));
            }

    return candidates;
}

int TabuSearch::calculateCost(const std::vector<int>& path) const
{
    if (path.size() == 0)
        throw std::invalid_argument("Invalid path");

    int cost = 0;

    for (size_t i = 0; i < path.size() - 1; i++)
        cost += _adjacencyMatrix[path[i]][path[i + 1]];

    return cost;
}

bool TabuSearch::isInPath(int vertex, const std::vector<int>& path) const
{
    return std::find(path.begin(), path.end(), vertex) != path.end();
}

bool TabuSearch::isInAspirationCriteria(int cost) const
{
    return cost < _bestSolution._cost;
}

void TabuSearch::setTimeLimit(std::chrono::seconds seconds)
{
    _timeLimit = seconds;
}