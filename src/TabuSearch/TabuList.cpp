#include "TabuList.hpp"

TabuList::TabuList(int size, int tenure) : _tenure(tenure)
{
    _tabuList.resize(size);
    for (auto& row : _tabuList)
        row.resize(size);
}

void TabuList::update()
{
    for (auto& row : _tabuList)
        for (auto& elem : row)
            if (elem != 0)
                elem--;
}

void TabuList::addTabu(const std::pair<int, int>& move)
{
    _tabuList[move.first][move.second] += _tenure;
}

void TabuList::divideBy(int divisor)
{
    for (auto& row : _tabuList)
        std::transform(row.begin(), row.end(), row.begin(), [&](auto& elem) { return elem /= divisor; });
}

bool TabuList::isTabu(const std::pair<int, int>& move) const
{
    return _tabuList[move.first][move.second] != 0;
}