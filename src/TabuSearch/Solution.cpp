#include "Solution.hpp"
#include "IAlgorithm.hpp"

Solution::Solution() : _cost(IAlgorithm::INF)
{
}

Solution::Solution(const std::vector<int>& path) :_cost(IAlgorithm::INF), _path(path)
{
}

Solution::Solution(const std::vector<int>& path, const std::pair<int, int>& move) : _cost(IAlgorithm::INF), _path(path), _move(move)
{
}

bool Solution::operator<(const Solution& other) const
{
    return _cost < other._cost;
}
