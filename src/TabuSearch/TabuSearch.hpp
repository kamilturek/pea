#pragma once
#include "IAlgorithm.hpp"
#include "RandomGenerator.hpp"
#include "Solution.hpp"
#include "TabuList.hpp"
#include "Timer.hpp"
#include <algorithm>

class TabuSearch : public IAlgorithm
{
public:
    explicit TabuSearch(const Matrix<int>& adjacencyMatrix);

    std::pair<int, std::vector<int>> solve() override;
    void setTimeLimit(std::chrono::seconds seconds);

private:
    std::chrono::seconds _timeLimit = std::chrono::seconds(1);
    // std::chrono::milliseconds _timeLimit = std::chrono::milliseconds(500);
    const int _diversificationLimit = 5000;
    int _iterationsSinceImprovement = 0;

    Matrix<int> _adjacencyMatrix;
    TabuList _tabuList;
    Solution _bestSolution;

    bool isInPath(int vertex, const std::vector<int>& path) const;
    bool isInAspirationCriteria(int cost) const;

    int calculateCost(const std::vector<int>& path) const;

    Solution getBestNeighbourSolution(const std::vector<int>& path);
    std::vector<int> generateGreedyPath() const;
    std::vector<int> generateGreedyRandomPath() const;
    std::vector<Solution> getCandidates(const std::vector<int>& path);
};