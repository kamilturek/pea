#pragma once
#include <vector>

class IMutation
{
public:
    virtual void mutate(std::vector<int>& individual, float mutationProbability) const = 0;
    virtual ~IMutation() {}
};