#include "SwapMutation.hpp"
#include "RandomGenerator.hpp"

void SwapMutation::mutate(std::vector<int>& individual, float mutationProbability) const
{
    RandomGenerator randomGenerator;

    for (size_t i = 0; i < individual.size(); i++)
    {
        if (randomGenerator.randomFloat(0, 1) < mutationProbability)
        {
            size_t j = randomGenerator.randomInt(0, individual.size() - 1);
            std::swap(individual[i], individual[j]);
        }
    }
}
