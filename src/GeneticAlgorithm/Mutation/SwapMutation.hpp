#include "IMutation.hpp"

class SwapMutation : public IMutation
{
public:
    void mutate(std::vector<int>& individual, float mutationProbability) const override;
};