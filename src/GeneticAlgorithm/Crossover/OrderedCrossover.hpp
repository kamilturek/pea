#pragma once
#include "ICrossover.hpp"

class OrderedCrossover : public ICrossover
{
public:
    std::pair<std::vector<int>, std::vector<int>> crossover(const std::vector<int>& parent1,
                                                            const std::vector<int>& parent2) override;

private:
    size_t _startPoint;
    size_t _endPoint;

    void selectCrossingPoints(size_t individualSize);
    void copyMatchingSections(const std::vector<int>& parent, std::vector<int>& child);
    void insertRemaining(const std::vector<int>& parent, std::vector<int>& child);
    bool contains(const std::vector<int>& vec, int element) const;
};