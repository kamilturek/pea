#pragma once
#include <vector>

class ICrossover
{
public:
    virtual std::pair<std::vector<int>, std::vector<int>> crossover(const std::vector<int>& parent1, const std::vector<int>& parent2) = 0;
    virtual ~ICrossover() {}
};