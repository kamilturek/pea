#include "OrderedCrossover.hpp"
#include "RandomGenerator.hpp"
#include <stdexcept>
#include <iostream>

std::pair<std::vector<int>, std::vector<int>> OrderedCrossover::crossover(const std::vector<int>& parent1,
                                                                          const std::vector<int>& parent2)
{
    if (parent1.size() != parent2.size())
        throw std::length_error("Parent sizes have to be equal.");

    selectCrossingPoints(parent1.size());

    std::vector<int> child1(parent1.size(), -1);
    std::vector<int> child2(parent2.size(), -1);

    copyMatchingSections(parent1, child2);
    copyMatchingSections(parent2, child1);

    insertRemaining(parent1, child1);
    insertRemaining(parent2, child2);
    
    return std::make_pair(child1, child2);
}


void OrderedCrossover::selectCrossingPoints(size_t individualSize)
{
    RandomGenerator randomGenerator;
    size_t a = randomGenerator.randomInt(0, individualSize);
    size_t b = randomGenerator.randomInt(0, individualSize);

    _startPoint = std::min(a, b);
    _endPoint = std::max(a, b);
}

void OrderedCrossover::copyMatchingSections(const std::vector<int>& parent, std::vector<int>& child)
{
    std::copy(parent.begin() + _startPoint, parent.begin() + _endPoint, child.begin() + _startPoint);
}

void OrderedCrossover::insertRemaining(const std::vector<int>& parent, std::vector<int>& child)
{
    size_t filledPositions = _endPoint - _startPoint;
    auto parentIterator = parent.begin() + _endPoint;
    auto childIterator = child.begin() + _endPoint;

    while (filledPositions < child.size())
    {
        if (parentIterator == parent.end())
            parentIterator = parent.begin();

        if (childIterator == child.end())
            childIterator = child.begin();

        if (!contains(child, *parentIterator))
        {
            *childIterator = *parentIterator;
            childIterator++;
            filledPositions++;
        }
        parentIterator++;
    }
}

bool OrderedCrossover::contains(const std::vector<int>& vec, int element) const
{
    return std::find(vec.begin(), vec.end(), element) != vec.end();
}