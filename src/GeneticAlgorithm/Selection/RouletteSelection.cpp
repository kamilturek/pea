#include "RouletteSelection.hpp"
#include "RandomGenerator.hpp"
#include <algorithm>
#include <iostream>
#include <numeric>

RouletteSelection::RouletteSelection()
{
}

std::vector<std::vector<int>>
RouletteSelection::selection(const std::vector<std::pair<float, std::vector<int>>>& rankedPopulation) const
{
    std::vector<float> individualProbs = probabilities(rankedPopulation);
    std::vector<std::vector<int>> newPopulation;

    for (size_t i = 0; i < rankedPopulation.size(); i++)
    {
        float random = RandomGenerator().randomFloat(0.0f, 1.0f);
        for (size_t j = 0; j < individualProbs.size(); j++)
            if (random <= individualProbs.at(j))
            {
                newPopulation.push_back(rankedPopulation.at(j).second);
                break;
            }
    }

    return newPopulation;
}

std::vector<float> RouletteSelection::probabilities(const std::vector<std::pair<float, std::vector<int>>>& rankedPopulation) const
{
    float fitnessSum = std::accumulate(rankedPopulation.cbegin(), rankedPopulation.cend(), 0.0f,
                                       [](float a, const std::pair<float, std::vector<int>>& b) { return a + b.first; });

    std::vector<float> relativeFitnesses;
    std::transform(rankedPopulation.cbegin(), rankedPopulation.cend(), std::back_inserter(relativeFitnesses),
                   [&](const auto& individual) { return individual.first / fitnessSum; });

    std::vector<float> probabilities;
    for (auto it = relativeFitnesses.begin(); it != relativeFitnesses.end(); it++)
    {
        if (it + 1 != relativeFitnesses.end())
            probabilities.push_back(std::accumulate(relativeFitnesses.begin(), it + 1, 0.0f));
        else
            probabilities.push_back(1);
    }

    return probabilities;
}