#pragma once
#include "ISelection.hpp"
#include <vector>

class RouletteSelection : public ISelection
{
public:
    RouletteSelection();
    std::vector<std::vector<int>> selection(const std::vector<std::pair<float, std::vector<int>>>& rankedPopulation) const override;

private:
    std::vector<float> probabilities(const std::vector<std::pair<float, std::vector<int>>>& rankedPopulation) const;
};