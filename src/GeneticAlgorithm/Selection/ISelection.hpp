#pragma once
#include <vector>

class ISelection
{
public:
    virtual std::vector<std::vector<int>> selection(const std::vector<std::pair<float, std::vector<int>>>& rankedPopulation) const = 0;
    virtual ~ISelection() {}
};