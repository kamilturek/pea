#include "GeneticAlgorithm.hpp"
#include "Fitness.hpp"
#include "RandomGenerator.hpp"
#include <algorithm>
#include <numeric>

GeneticAlgorithm::GeneticAlgorithm(const Matrix<int>& adjacencyMatrix) : _adjacencyMatrix(adjacencyMatrix)
{
    createInitialPopulation();
}

std::pair<int, std::vector<int>> GeneticAlgorithm::solve()
{
    auto rankedRoutes = rankRoutes();
    auto matingPool = _selectionMethod->selection(rankedRoutes);
    auto breed = breedPopulation(matingPool);

    return std::make_pair(0, std::vector<int>());
}

std::vector<std::vector<int>> GeneticAlgorithm::breedPopulation(const std::vector<std::vector<int>>& matingPool) const
{
    RandomGenerator randomGenerator;
    std::vector<std::vector<int>> children;

    while (children.size() < _populationSize)
    {
        const auto& parent1 = matingPool[randomGenerator.randomInt(0, matingPool.size() - 1)];
        const auto& parent2 = matingPool[randomGenerator.randomInt(0, matingPool.size() - 1)];
        auto childrenPair = _crossoverMethod->crossover(parent1, parent2);
        children.push_back(childrenPair.first);
        children.push_back(childrenPair.second);
    }

    return children;
}

std::vector<std::pair<float, std::vector<int>>> GeneticAlgorithm::rankRoutes() const
{
    std::vector<std::pair<float, std::vector<int>>> fitnessResults;

    for (const auto route : _population)
    {
        float fitness = Fitness(route, _adjacencyMatrix).routeFitness();
        fitnessResults.push_back(std::make_pair(fitness, route));
    }

    std::sort(fitnessResults.begin(), fitnessResults.end(), [](const auto& a, const auto& b) { return a.first > b.first; });

    return fitnessResults;
}

void GeneticAlgorithm::createInitialPopulation()
{
    _population.clear();

    for (size_t i = 0; i < _populationSize; i++)
        _population.push_back(createRoute(_adjacencyMatrix.size()));
}

std::vector<int> GeneticAlgorithm::createRoute(int routeSize) const
{
    std::vector<int> route(routeSize);

    std::iota(route.begin(), route.end(), 0);
    std::random_shuffle(route.begin(), route.end());

    return route;
}