#pragma once
#include "IAlgorithm.hpp"
#include "OrderedCrossover.hpp"
#include "RouletteSelection.hpp"
#include "Matrix.hpp"
#include <memory>
#include <vector>

class GeneticAlgorithm : public IAlgorithm
{
public:
    explicit GeneticAlgorithm(const Matrix<int>& adjacencyMatrix);
    std::pair<int, std::vector<int>> solve() override;

private:
    size_t _populationSize = 10;
    std::unique_ptr<ISelection> _selectionMethod = std::make_unique<RouletteSelection>();
    std::unique_ptr<ICrossover> _crossoverMethod = std::make_unique<OrderedCrossover>();

    Matrix<int> _adjacencyMatrix;
    std::vector<std::vector<int>> _population;

    std::vector<std::vector<int>> breedPopulation(const std::vector<std::vector<int>>& matingPool) const;    
    std::vector<std::pair<float, std::vector<int>>> rankRoutes() const;

    void createInitialPopulation();
    std::vector<int> createRoute(int routeSize) const;
};