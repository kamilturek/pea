#include "gtest/gtest.h"
#include "MatrixFixture.hpp"
#include "RouletteSelection.hpp"

TEST_F(MatrixFixture, RouletteSelectionTest)
{
    std::vector<std::pair<float, std::vector<int>>> rankedPopulation = {std::make_pair<float, std::vector<int>>(5.1f, {}), std::make_pair<float, std::vector<int>>(2.0f, {}), std::make_pair<float, std::vector<int>>(2.5f, {})};
    auto selection = RouletteSelection(rankedPopulation).selection();

    ASSERT_EQ(selection.size(), rankedPopulation.size());
}