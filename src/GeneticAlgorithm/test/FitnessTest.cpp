#include "gtest/gtest.h"
#include "MatrixFixture.hpp"
#include "Fitness.hpp"

TEST_F(MatrixFixture, RouteDistanceTest)
{
    const std::vector<int> route = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    ASSERT_EQ(Fitness(route, adjacencyMatrix).routeDistance(), 10);
}

TEST_F(MatrixFixture, RouteFitnessTest)
{
    const std::vector<int> route = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    ASSERT_FLOAT_EQ(Fitness(route, adjacencyMatrix).routeFitness(), 0.1f);
}