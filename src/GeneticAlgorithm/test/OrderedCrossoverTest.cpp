#include "OrderedCrossover.hpp"
#include "gtest/gtest.h"
#include <set>

TEST(OrderedCrossoverTest, DifferentParentSizes)
{
    std::vector<int> a = { 4, 5, 6, 7, 8, 9 };
    std::vector<int> b = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };

    ASSERT_THROW(OrderedCrossover().crossover(a, b), std::length_error);
}

TEST(OrderedCrossoverTest, UniqueElements)
{
    std::vector<int> a = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    std::vector<int> b = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };

    std::pair<std::vector<int>, std::vector<int>> children = OrderedCrossover().crossover(a, b);

    ASSERT_EQ(std::set<int>(children.first.begin(), children.first.end()).size(), a.size());
    ASSERT_EQ(std::set<int>(children.second.begin(), children.second.end()).size(), b.size());
}