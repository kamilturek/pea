#include "SwapMutation.hpp"
#include "gtest/gtest.h"

TEST(SwapMutationTest, UniqueElements)
{
    std::vector<int> individual = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    SwapMutation().mutate(individual, 1.0f);

    ASSERT_EQ(std::set<int>(individual.begin(), individual.end()).size(), individual.size());
}