#pragma once
#include "Matrix.hpp"
#include <vector>

class Fitness
{
public:
    Fitness(const std::vector<int>& route, const Matrix<int>& adjacencyMatrix);

    int routeDistance();
    float routeFitness();

private:
    int _distance;
    float _fitness;
    std::vector<int> _route;
    Matrix<int> _adjacencyMatrix;
};