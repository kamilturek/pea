#include "Fitness.hpp"

Fitness::Fitness(const std::vector<int>& route, const Matrix<int>& adjacencyMatrix)
    : _distance(0), _fitness(0.0f), _route(route), _adjacencyMatrix(adjacencyMatrix)
{
}

int Fitness::routeDistance()
{
    if (_distance == 0)
    {
        int pathDistance = 0;
        for (size_t i = 0; i < _route.size() - 1; i++)
            pathDistance += _adjacencyMatrix[_route[i]][_route[i + 1]];

        pathDistance += _adjacencyMatrix[_route.back()][_route.front()];

        _distance = pathDistance;
    }

    return _distance;
}

float Fitness::routeFitness()
{
    if (_fitness == 0.0f)
        _fitness = 1 / static_cast<float>(routeDistance());
    return _fitness;
}
