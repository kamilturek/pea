#pragma once
#include "Matrix.hpp"
#include <vector>

class IAlgorithm
{
public:
    static constexpr int INF = 9999;

    virtual std::pair<int, std::vector<int>> solve() = 0;
    virtual ~IAlgorithm()
    {
    }
};