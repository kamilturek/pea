#include "MatrixFileInputFixture.hpp"

TEST_F(MatrixFileInputFixture, OpeningExistingFileDoesNotThrowException)
{
    ASSERT_NO_THROW(MatrixFileInput(_testFilename, 0, 0));
}

TEST_F(MatrixFileInputFixture, ThrowsExceptionIfLinesNumberIsNegative)
{
    ASSERT_THROW(MatrixFileInput(_testFilename, -5, 0), std::invalid_argument);
}

TEST_F(MatrixFileInputFixture, ThrowsExceptionIfMatrixSizeIsNegative)
{
    ASSERT_THROW(MatrixFileInput(_testFilename, 0, -5), std::invalid_argument);
}

TEST_F(MatrixFileInputFixture, ThrowsExceptionIfFileDoesNotHaveEnoughElements)
{
    ASSERT_THROW(MatrixFileInput(_testFilename, 7, 40).readMatrix(), std::invalid_argument);
}

TEST(MatrixFileInputTest, OpeningNotExistingFileThrowsException)
{
    ASSERT_THROW(MatrixFileInput("not_existing.atsp", 0, 0), std::invalid_argument);
}

TEST(MatrixFileInputTest, ReadingMatrixFromFileGivesMatrixWithProperDimension)
{
    MatrixFileInput input("../test_data/ATSP/br17.atsp", 7, 17);
    auto matrix = input.readMatrix();

    ASSERT_EQ(matrix.size(), 17);
    ASSERT_EQ(matrix[0].size(), 17);
}

TEST(MatrixFileInputTest, ReadingMatrixFromFileGivesProperMatrixElements)
{
    MatrixFileInput input("../test_data/ATSP/br17.atsp", 7, 17);
    auto matrix = input.readMatrix();

    ASSERT_THAT(matrix[0], ::testing::ElementsAre(9999, 3, 5, 48, 48, 8, 8, 5, 5, 3, 3, 0, 3, 5, 8, 8, 5));
    ASSERT_THAT(matrix[4], ::testing::ElementsAre(48, 48, 74, 0, 9999, 6, 6, 12, 12, 48, 48, 48, 48, 74, 6, 6, 12));
}