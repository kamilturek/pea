#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Matrix.hpp"
#include "Node.hpp"

TEST(NodeTest, ComputesCorrectCost_1)
{
    Matrix<int> adjacencyMatrix({{9999, 20, 30, 10, 11},
                                 {15, 9999, 16, 4, 2},
                                 {3, 5, 9999, 2, 4},
                                 {19, 6, 18, 9999, 3},
                                 {16, 4, 7, 16, 9999}});

    Node node(adjacencyMatrix, {}, 0, 0, -1, 0);
    ASSERT_EQ(node.getCost(), 25);
    ASSERT_THAT(node.getReducedMatrix(), ::testing::ElementsAre(std::vector({9999, 10, 17, 0, 1}),
                                                                std::vector({12, 9999, 11, 2, 0}),
                                                                std::vector({0, 3, 9999, 0, 2}),
                                                                std::vector({15, 3, 12, 9999, 0}),
                                                                std::vector({11, 0, 0, 12, 9999})));
}

TEST(NodeTest, ComputesCorrectCost_2)
{
    Matrix<int> adjacencyMatrix({{9999, 10, 17, 0, 1},
                                 {12, 9999, 11, 2, 0},
                                 {0, 3, 9999, 0, 2},
                                 {15, 3, 12, 9999, 0},
                                 {11, 0, 0, 12, 9999}});

    Node node(adjacencyMatrix, {}, 0, 1, 0, 1);
    ASSERT_EQ(node.getCost(), 0);
    ASSERT_THAT(node.getReducedMatrix(), ::testing::ElementsAre(std::vector({9999, 9999, 9999, 9999, 9999}),
                                                                std::vector({9999, 9999, 11, 2, 0}),
                                                                std::vector({0, 9999, 9999, 0, 2}),
                                                                std::vector({15, 9999, 12, 9999, 0}),
                                                                std::vector({11, 9999, 0, 12, 9999})));
}

TEST(NodeTest, ComparesTwoNodesByGreaterOperator)
{
    Matrix<int> adjacencyMatrix_1({{9999, 20, 30, 10, 11},
                                 {15, 9999, 16, 4, 2},
                                 {3, 5, 9999, 2, 4},
                                 {19, 6, 18, 9999, 3},
                                 {16, 4, 7, 16, 9999}});

    Node node_1(adjacencyMatrix_1, {}, 0, 0, -1, 0);

    Matrix<int> adjacencyMatrix_2({{9999, 10, 17, 0, 1},
                                 {12, 9999, 11, 2, 0},
                                 {0, 3, 9999, 0, 2},
                                 {15, 3, 12, 9999, 0},
                                 {11, 0, 0, 12, 9999}});

    Node node_2(adjacencyMatrix_2, {}, 0, 1, 0, 1);

    ASSERT_TRUE(node_1 > node_2);
    ASSERT_FALSE(node_2 > node_1);
}