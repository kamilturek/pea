#include "BranchAndBound.hpp"
#include "MatrixFileInput.hpp"
#include "Node.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

TEST(BranchAndBoundTest, SolvesProblemExactly_4)
{
    BranchAndBound bnb(MatrixFileInput("../test_data/SMALL/data10.txt", 2, 10).readMatrix());
    ASSERT_EQ(bnb.solve().first, 212);
}

TEST(BranchAndBoundTest, SolvesProblemExactly_5)
{
    BranchAndBound bnb(MatrixFileInput("../test_data/SMALL/data11.txt", 2, 11).readMatrix());
    ASSERT_EQ(bnb.solve().first, 202);
}

TEST(BranchAndBoundTest, SolvesProblemExactly_6)
{
    BranchAndBound bnb(MatrixFileInput("../test_data/TUREK/tsp_10a.txt", 1, 10).readMatrix());

    std::pair<int, std::vector<int>> result = bnb.solve();
    ASSERT_EQ(result.first, 10);
    ASSERT_THAT(result.second, ::testing::ElementsAre(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0));
}