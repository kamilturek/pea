#include "gtest/gtest.h"
#include "AlgorithmFactory.hpp"

TEST(AlgorithmFactoryTest, CreatesProperAlgorithm)
{
    ASSERT_EQ(typeid(*AlgorithmFactory().createAlgorithm(AlgorithmFactory::Algorithm::BruteForce, {{1}})), typeid(BruteForce));
}

TEST(AlgorithmFactoryTest, ReturnsNullptrIfIncorrectAlgorithmWasPassed)
{
    ASSERT_THROW(AlgorithmFactory().createAlgorithm(static_cast<AlgorithmFactory::Algorithm>(99), {{1}}), std::out_of_range);
}