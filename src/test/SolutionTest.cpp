#include "Solution.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

TEST(SolutionTest, LessThanOperator_1)
{
    Solution solution1;
    Solution solution2;

    solution1._cost = 10;
    solution2._cost = 120;

    ASSERT_TRUE(solution1 < solution2);
}

TEST(SolutionTest, LessThanOperator_2)
{
    Solution solution1;
    Solution solution2;

    solution1._cost = -120;
    solution2._cost = -500;

    ASSERT_FALSE(solution1 < solution2);
}

TEST(SolutionTest, AssignmentOperator)
{
    Solution solution1({ 0, 1, 2, 3 }, { 1, 2 });
    Solution solution2({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 7, 8 });
    solution1._cost = 10;
    solution2._cost = 20;

    solution2 = solution1;

    ASSERT_THAT(solution2._path, ::testing::ElementsAre(0, 1, 2, 3));
    ASSERT_EQ(solution2._move, std::make_pair(1, 2));
    ASSERT_EQ(10, solution2._cost);
}