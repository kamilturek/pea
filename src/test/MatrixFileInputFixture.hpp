#include <fstream>
#include <cstdio>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "MatrixFileInput.hpp"

class MatrixFileInputFixture : public ::testing::Test
{
protected:
    std::string _testFilename = "matrix_input_fixture_file.txt";

    void SetUp() override
    {
        std::ofstream output(_testFilename);

        for (int i = 1; i <= 5; i++)
            output << i << '\n';
    }

    void TearDown() override
    {
        std::remove(_testFilename.c_str());
    }
};