#include "BruteForce.hpp"
#include "MatrixFileInput.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

TEST(BruteForceTest, SolvesProblemExactly_1)
{
    BruteForce bruteForce(MatrixFileInput("../test_data/TUREK/tsp_10a.txt", 1, 10).readMatrix());
    std::pair<int, std::vector<int>> result = bruteForce.solve();
    ASSERT_EQ(result.first, 10);
    ASSERT_THAT(result.second, ::testing::ElementsAre(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0));
}

TEST(BruteForceTest, SolvesProblemExactly_2)
{
    BruteForce bruteForce(MatrixFileInput("../test_data/TUREK/tsp_10b.txt", 1, 10).readMatrix());
    std::pair<int, std::vector<int>> result = bruteForce.solve();
    ASSERT_EQ(result.first, 10);
}

TEST(BruteForceTest, SolvesProblemExactly_3)
{
    BruteForce bruteForce(MatrixFileInput("../test_data/TUREK/tsp_10c.txt", 1, 10).readMatrix());
    std::pair<int, std::vector<int>> result = bruteForce.solve();
    ASSERT_EQ(result.first, 10);
}

TEST(BruteForceTest, ThrowsIfPassedNonSquareMatrix)
{
    ASSERT_THROW(BruteForce({ { 1, 2, 3, 4 }, { 0, 0, 0, 0 } }), std::invalid_argument);
}