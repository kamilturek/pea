#pragma once
#include "IAlgorithm.hpp"
#include "Matrix.hpp"
#include <algorithm>
#include <limits>
#include <numeric>
#include <vector>
#include <tuple>

class BruteForce : public IAlgorithm
{
public:
    explicit BruteForce(const Matrix<int>& adjacencyMatrix);
    std::pair<int, std::vector<int>> solve() override;

private:
    Matrix<int> _adjacencyMatrix;

    int calculateCost(const std::vector<int>& path) const;
};