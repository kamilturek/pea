#include "BruteForce.hpp"

BruteForce::BruteForce(const Matrix<int>& adjacencyMatrix)
    : _adjacencyMatrix(adjacencyMatrix)
{
    if (adjacencyMatrix.size() == 0 || std::any_of(adjacencyMatrix.begin(), adjacencyMatrix.end(),
                                                   [&](const auto& row) { return row.size() != adjacencyMatrix.size(); }))
        throw std::invalid_argument("BŁĄD: Niepoprawna macierz.");
}

std::pair<int, std::vector<int>> BruteForce::solve()
{
    const int citiesCount = _adjacencyMatrix.size();
    const int startCity = 0;

    int bestCost = std::numeric_limits<int>::max();
    std::vector<int> bestPath;

    std::vector<int> vertices(citiesCount - 1);
    std::iota(vertices.begin(), vertices.end(), startCity + 1);

    do
    {
        std::vector<int> path({ startCity, startCity });
        path.insert(path.begin() + 1, vertices.begin(), vertices.end());

        const int pathCost = calculateCost(path);
        if (pathCost < bestCost)
        {
            bestCost = pathCost;
            bestPath = path;
        }
    } while (std::next_permutation(vertices.begin(), vertices.end()));

    return std::make_pair(bestCost, bestPath);
}

int BruteForce::calculateCost(const std::vector<int>& path) const
{
    int cost = 0;

    for (size_t i = 0; i < path.size() - 1; i++)
        cost += _adjacencyMatrix[path[i]][path[i + 1]];

    return cost;
}