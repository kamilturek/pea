#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo ">> Illegal number of parameters"
    echo ">> Usage: analysis.sh <project_dir>"
    exit 1
fi

function check_return_code {
    if [ $? -ne 0 ]; then
        echo ">> An error occured during analysis. Check logs above."
        exit 1
    fi
}

chmod +x $1/scripts/valgrind.sh
$1/scripts/valgrind.sh $1
check_return_code

chmod +x $1/scripts/cppcheck.sh
$1/scripts/cppcheck.sh $1
check_return_code

echo ">> All analysis have passed without errors."