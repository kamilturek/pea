#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    echo "Usage: cppcheck.sh <project_dir>"
    exit 1
fi

cppcheck $1/src/*.cpp $1/src/*/*.cpp --enable=all --suppressions-list=suppressions.txt --error-exitcode=1
exit $?