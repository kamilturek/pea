#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    echo "Usage: valgrind.sh <project_dir>"
    exit 1
fi

valgrind --leak-check=full --error-exitcode=1 $1/build/RunUnitTests
exit $?