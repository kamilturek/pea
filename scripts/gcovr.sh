#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    echo "Usage: gcovr.sh <project_dir>"
    exit 1
fi

gcovr -e $1/dependencies -e ".*Test.*" -e ".*Fixture.*" -r $1