[![pipeline status](https://gitlab.com/kamilturek/pea/badges/master/pipeline.svg)](https://gitlab.com/kamilturek/pea/commits/master)
[![coverage report](https://gitlab.com/kamilturek/pea/badges/master/coverage.svg?job=test)](https://gitlab.com/kamilturek/pea/commits/master)

# PEA

Project for Design of Effective Algorithms classes.

## How to clone?
```
git clone https://gitlab.com/kamilturek/pea.git
git submodule update --init --recursive
```