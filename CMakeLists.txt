cmake_minimum_required (VERSION 2.6)
set(CMAKE_CXX_FLAGS "-pthread -Wall -Wextra -pedantic -Wshadow -Wnon-virtual-dtor -Wold-style-cast -Wnull-dereference")
set(CMAKE_CXX_STANDARD 17)

if(NOT CMAKE_BUILD_TYPE)
   set(CMAKE_BUILD_TYPE Debug)
endif()

if(CMAKE_BUILD_TYPE MATCHES Debug)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -g")
    message(">> Build type: Debug")
elseif(CMAKE_BUILD_TYPE MATCHES Release)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3")
    message(">> Build type: Release")
endif()
#####################################
# Build Target
#####################################
set(BUILD_SOURCES
    src/AlgorithmFactory.cpp
    src/BruteForce/BruteForce.cpp
    src/BranchAndBound/BranchAndBound.cpp
    src/BranchAndBound/Node.cpp
    src/ConsoleUI.cpp
    src/DynamicProgramming/DynamicProgramming.cpp
    src/GeneticAlgorithm/Fitness.cpp
    src/GeneticAlgorithm/GeneticAlgorithm.cpp
    src/GeneticAlgorithm/Crossover/OrderedCrossover.cpp
    src/GeneticAlgorithm/Mutation/SwapMutation.cpp
    src/GeneticAlgorithm/Selection/RouletteSelection.cpp
    src/main.cpp
    src/MatrixFileInput.cpp
    src/MatrixGenerator.cpp
    src/RandomGenerator.cpp
    src/TabuSearch/Solution.cpp
    src/TabuSearch/TabuList.cpp
    src/TabuSearch/TabuSearch.cpp
    src/TabuSearch/MultithreadedTabuSearch.cpp
    src/Timer.cpp
)

add_executable(Run ${BUILD_SOURCES})
target_include_directories(Run PRIVATE
                            src/
                            src/BruteForce
                            src/BranchAndBound
                            src/DynamicProgramming
                            src/GeneticAlgorithm
                            src/GeneticAlgorithm/Selection
                            src/GeneticAlgorithm/Crossover
                            src/TabuSearch
)

#####################################
# Unit Tests
#####################################
add_subdirectory(dependencies/googletest)
enable_testing()

set(TEST_SOURCES
    src/AlgorithmFactory.cpp
    src/BranchAndBound/BranchAndBound.cpp
    src/BranchAndBound/Node.cpp
    src/BruteForce/BruteForce.cpp
    src/DynamicProgramming/DynamicProgramming.cpp
    src/GeneticAlgorithm/Fitness.cpp
    src/GeneticAlgorithm/GeneticAlgorithm.cpp
    src/GeneticAlgorithm/Mutation/SwapMutation.cpp
    src/GeneticAlgorithm/Selection/RouletteSelection.cpp
    src/GeneticAlgorithm/Crossover/OrderedCrossover.cpp
    src/GeneticAlgorithm/test/FitnessTest.cpp
    src/GeneticAlgorithm/test/GeneticAlgorithmTest.cpp
    src/GeneticAlgorithm/test/OrderedCrossoverTest.cpp
    src/GeneticAlgorithm/test/SwapMutationTest.cpp
    src/MatrixFileInput.cpp
    src/RandomGenerator.cpp
    src/TabuSearch/MultithreadedTabuSearch.cpp
    src/TabuSearch/Solution.cpp
    src/TabuSearch/TabuList.cpp
    src/TabuSearch/TabuSearch.cpp
    src/Timer.cpp
    src/test/AlgorithmFactoryTest.cpp
    src/test/BranchAndBoundTest.cpp
    src/test/BruteForceTest.cpp
    src/test/GeneticAlgorithmTest.cpp
    src/test/MatrixFileInputTest.cpp
    src/test/NodeTest.cpp
    src/test/SolutionTest.cpp
)

add_executable(RunUnitTests ${TEST_SOURCES})
target_link_libraries(RunUnitTests gtest gtest_main gmock gcov)
target_include_directories(RunUnitTests PRIVATE
                            src/
                            src/BruteForce
                            src/BranchAndBound
                            src/DynamicProgramming
                            src/GeneticAlgorithm
                            src/GeneticAlgorithm/Crossover
                            src/GeneticAlgorithm/Mutation
                            src/GeneticAlgorithm/Selection
                            src/TabuSearch
                            src/test/fixtures
)
set_target_properties(RunUnitTests PROPERTIES COMPILE_FLAGS "-fprofile-arcs -ftest-coverage -g -O0" )